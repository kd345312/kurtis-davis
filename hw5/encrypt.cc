#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
using namespace std;

int main(int argc,char *argv[]) {
  string key;

  cin >> key; // Assuming key has no spaces
  ifstream fin;
  fin.open(argv[1]);

  if (fin.fail()) {
    cout << "Failed to open " << argv[1] << endl;
    exit(-1);
  }
  int count =0;
  while (!fin.eof()) {
    char c;
    c = fin.get();
    if (!fin.fail()) {
      cout << char(c^key[count%key.size()]);
    }
    count++;
  }
}
