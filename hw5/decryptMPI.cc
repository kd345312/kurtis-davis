#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <mpi.h>
using namespace std;

int main(){
    int num_processes; // both required for openMPI
    int proc_num;
    vector<string> words;
    vector<char> cipher;
    vector<int> stdFreq;
    string word, key;
    char c;
    MPI_Init(int *argc, char **argv); // takes argc and argv arguements
    MPI_Comm_size(MPI_COMM_WORLD, &num_processes); // returns size of group assoc. w/ communicator
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_num);      // determines rank of process in communicator
    ifstream fin;
    int stdSize = 0, num, junk, count = 0;
    fin.open(argv[1]);
    if(fin.fail()){
        cout << "failed to open\n";
        exit(-1);
    }
    while(fin >> word){
        words.push_back(word);
    }
    fin.close();
    fin.open(argv[2]);
    if(fin.fail()){
        cout << "failed to open\n";
        exit(-1);
    }
    fin >> num;
    stdSize = num;
    while(fin >> num){
        fin >> num;
        stdFreq.push_back(num);
    }
    fin.close();
    fin.open(argv[3]);
    if(fin.fail()){
        cout << "failed to open\n";
        exit(-1);
    }
    while(fin >> c){
        cipher.push_back(c);
    }
    fin.close();
    for(vector<string>::iterator it = words.begin(); it != words.end(); it++){
        key = *it;
        for(vector<char>::iterator it2 = cipher.begin(); it2 != cipher.end(); it2++){
            c = *it2;
            cout << char(c^key[count%key.size()]);
            count++;
        }
        cout << "\n\n";
    }
    MPI_Finalize(); // Required for OpenMPI
}